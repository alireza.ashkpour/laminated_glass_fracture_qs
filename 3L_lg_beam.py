import fenics as fe
import numpy as np
import matplotlib.pyplot as plt
import math
import os
import utils.functions as fu


# Mesh creator
def prepare_mesh():
    mesh = fe.Mesh("Meshes/QS_4PB_1D_ref.xml")
    return mesh


def append_file(folder, file_name, files):
    file_temp = fe.XDMFFile(folder + "/" + file_name)
    file_temp.parameters["flush_output"] = True
    files.append(file_temp)


def prepare_files(folder, params, n_glass, n_layers):
    # Post-process files
    # File for displacement field
    files_u = []
    for i in range(n_layers):
        append_file(folder, "displ_u_" + str(i) + ".xdmf", files_u)
        append_file(folder, "displ_w_" + str(i) + ".xdmf", files_u)
        append_file(folder, "displ_phi_" + str(i) + ".xdmf", files_u)

    # File for damage field
    files_d = []
    for i in range(n_glass):
        append_file(folder, "damage_" + str(i) + ".xdmf", files_d)

    # Create log_file
    if not os.path.exists(folder):
        os.makedirs(folder)
    log_file = open(folder + "/log_file.txt", "w+")
    log_file.write("Log_file.txt\n")
    log_file.write(
        "eps_staggered = " + str(params.tol_sl) + ", max_iters_staggered = " + str(params.max_iter) +
        ", criterium = " + str(params.conv_crit) + ", True = converges\n")
    log_file.write("time\titer_numbers\tconverges\n")
    log_file.close()

    # Create stress_file
    stress_file = open(folder + "/stress_data.txt", "w+")
    stress_file.write("Stress_data.txt\n")
    stress_file.write(
        "S = Stress, E = Strain, T=Top surface in middle of beam, B=Bottom surface in middle of beam, TQ=Top surface in quarter ob beam\n")
    stress_file.write("time\tu_B\tE_x_B\tS_x_B\tE_x_T\tS_x_T\tE_x_QT\tS_x_QT\n")
    stress_file.close()

    # Create reacts_file
    react_file = open(folder + "/reaction_data.txt", "w+")
    react_file.write("Reaction_data.txt\n")
    react_file.write("time\tu_B\treaction\n")
    react_file.close()

    return files_u, files_d


def glass_3l_lg_solver(time_space, ft, folder, params, vis_mat, type_active):

    # Definition of supporting point
    def left_point(x):
        return fe.near(x[0], l_off)

    # Definition of loading point
    def left_load_point(x):
        return fe.near(x[0], l_x / 2 - l_0 / 2)

    # Definition of symmetry axis location
    def symmetry_axis(x, on_boundary):
        return fe.near(x[0], l_x/2) and on_boundary

    # Staggered loop
    def solve_staggered():
        # Time loop
        en_prev = 0.0
        for i in range(0, len(time_space)):
            t = time_space[i]

            u_d.t = t

            # Update of foil stiffness
            update_foil(t)

            print(Es[1])
            print(Gs[1].g)

            # Informative print of time instant
            print("Time instant: ", t)

            ite = 1
            err = 50.0

            converges = False

            # Staggered loop
            while err > params.tol_sl:

                # Damage and displacement solutions
                solve_displacement()

                solve_damage()

                if params.conv_crit == "rel_inc":
                    # Damage and displacement increments
                    du.assign(u - u_old)
                    dd.assign(d - d_old)

                    du_split = du.split(deepcopy=True)
                    u_split = u.split(deepcopy=True)
                    err_u = fe.norm(du_split[1]) / fe.norm(u_split[1])

                    # Errors - damage is not standardized
                    #err_u = fe.norm(du) / fe.norm(u)
                    err_d = fe.norm(dd)
                    err = max(err_u, err_d)

                    print("iter", ite, "errors", err_u, err_d)

                    u_old.assign(u)
                    d_old.assign(d)
                elif params.conv_crit == "energy":
                    # en = get_total_energy(u, d)
                    # err = abs((en - en_prev) / en)
                    # print("iter", ite, ", energy_error: ", err)
                    # en_prev = en
                    raise Exception("Energy criterium not implement yet!")
                else:
                    raise Exception("conv_crit must be energy/rel_inc!")

                ite += 1

                # Max iterations condition
                if ite > params.max_iter:
                    print("max iterations reached")
                    break

                if err <= params.tol_sl:
                    converges = True

            d_min.assign(d)

            save_displacement_and_damage(file_u, file_d, t, n_lay, glass_num)

            save_stress_reaction_sd(t)

            file_object = open(folder + "/log_file.txt", "a")
            file_object.write(str(round(t, 5)) + "\t" + str(ite) + "\t" + str(converges) + "\n")
            file_object.close()

            d_split = d.split(deepcopy=True)
            if np.amax(d_split[0].vector()[:]) > 0.99 and np.amax(d_split[1].vector()[:]) > 0.99:
                break

        [file_i.close() for file_i in file_u]
        [file_i.close() for file_i in file_d]

    # Displacement solver
    def solve_displacement():
        u.vector()[:] = np.random.rand(u.vector().size())
        H = fe.derivative(u_form, u, fe.TrialFunction(V))
        problem = fe.NonlinearVariationalProblem(u_form, u, BC_u, H)
        solver = fe.NonlinearVariationalSolver(problem)
        parameters = {"newton_solver": {"error_on_nonconvergence": False,
                                        "relative_tolerance": params.tol_nm}}
        solver.parameters.update(parameters)
        solver.solve()

    # Damage snes variational inequalities solver based on Newton's method
    def solve_damage():
        lower = d_min
        upper = fe.interpolate(fe.Constant((1.0, 1.0)), W)
        #upper = fe.interpolate(fe.Constant(1.0), W)

        # Solution of damage formulationw
        H = fe.derivative(d_form, d, fe.TrialFunction(W))

        snes_solver_parameters = {"nonlinear_solver": "snes",
                                  "snes_solver": {"linear_solver": "lu",
                                                  "relative_tolerance": 1.0e-6,
                                                  "absolute_tolerance": 1.0e-6,
                                                  "maximum_iterations": 50,
                                                  "report": True,
                                                  "error_on_nonconvergence": False,
                                                  "line_search": "basic"}}

        problem = fe.NonlinearVariationalProblem(d_form, d, BC_d, H)
        problem.set_bounds(lower, upper)

        solver = fe.NonlinearVariationalSolver(problem)
        solver.parameters.update(snes_solver_parameters)
        solver.solve()

    # Save displacements and damages into files
    def save_displacement_and_damage(file_u_i, file_d_i, t_i, n_layers, n_glass):
        u_ = u.split(deepcopy=True)
        for i in range(n_layers):
            file_u_i[3*i].write(u_[3*i], t_i)
            file_u_i[3*i + 1].write(u_[3*i + 1], t_i)
            file_u_i[3*i + 2].write(u_[3*i + 2], t_i)

        d_ = d.split(deepcopy=True)
        for i in range(n_glass):
            file_d_i[i].write(d_[i], t_i)

    # Save stress and reaction to file
    def save_stress_reaction_sd(t_i):
        stress_file = open(folder + "/stress_data.txt", "a")
        lg_displ = u.split(deepcopy=True)
        u_bottom = lg_displ[0]
        u_top = lg_displ[6]
        phi_bottom = lg_displ[2]
        phi_top = lg_displ[8]
        eps_bottom = u_bottom.dx(0) + phi_bottom.dx(0)*(-0.5*h[0])
        eps_top = u_top.dx(0) + phi_top.dx(0)*(0.5*h[2])
        strain_bottom = fu.local_project(eps_bottom, V0)
        strain_top = fu.local_project(eps_top, V0)
        stress_bottom = fu.local_project((1.0 - d[0])**2*Es[0]*fu.mc_bracket(eps_bottom) - Es[0]*fu.mc_bracket(-eps_bottom), V0)
        stress_top = fu.local_project((1.0 - d[1])**2*Es[2]*fu.mc_bracket(eps_top) - Es[2]*fu.mc_bracket(-eps_top), V0)
        s1 = strain_bottom(p_bottom)
        st1 = stress_bottom(p_bottom)
        s2 = strain_top(p_top)
        st2 = stress_top(p_top)
        s3 = 0.0
        st3 = 0.0
        disp = lg_displ[1](p_top)
        stress_file.write(str(t_i) + "\t" + str(disp) + "\t" + str(s1) + "\t" + str(st1) + "\t" + str(s2) + "\t" + str(st2) + "\t" + str(s3) + "\t" + str(st3) + "\n")
        stress_file.close()
        react_file = open(folder + "/reaction_data.txt", "a")
        forces = fe.assemble(u_form)
        react = forces[r_dof]
        react_file.write(str(t_i) + "\t" + str(disp) + "\t" + str(react) + "\n")
        react_file.close()

    # Calculate new lambda and mu for foil
    def update_foil(t_i):
        G_foil_t = get_G(t_i*60.0/1.8/2)
        G_foil.g = G_foil_t

    # Get foil stiffness for given time t_i
    def get_G(t_i):
        a_t = math.pow(10.0, -c1*(T_act - T_ref)/(c2 + T_act - T_ref))
        temp = G_inf
        n = len(G)
        temp += sum(G[k]*math.exp(-t_i/(a_t*theta[k])) for k in range(0, n))
        return temp

    # Displacement form getter
    def get_u_form():
        # Selective integration for shearlock reduction
        dx_shear = fe.dx(metadata={"quadrature_degree": 0})

        lg_trial = fe.split(u)
        lg_test = fe.TestFunctions(V)

        u_form = 0.0

        A = [b * hi for hi in h]
        I = [1.0 / 12.0 * b * hi ** 3 for hi in h]

        glass_ind = 0

        for i in range(n_lay):

            ind = 3*i
            ind_max = 3*n_lay

            u_tr, w_tr, phi_tr = lg_trial[ind], lg_trial[ind + 1], lg_trial[ind + 2]
            u_test, w_test, phi_test = lg_test[ind], lg_test[ind + 1], lg_test[ind + 2]

            if i % 2 == 0:
                hs = np.linspace(-0.5*h[i], 0.5*h[i], n_ni)
                dh = abs(hs[1] - hs[0])
                for j in range(len(hs) - 1):
                    gauss = 0.5*(hs[j] + hs[j + 1])
                    eps_z = u_tr.dx(0) + phi_tr.dx(0)*gauss
                    d_eps_z = u_test.dx(0) + phi_test.dx(0)*gauss
                    eps_z_p = fu.mc_bracket(eps_z)
                    eps_z_n = -fu.mc_bracket(-eps_z)
                    u_form += fe.inner((1.0 - d[glass_ind])**2*Es[i]*eps_z_p + Es[i]*eps_z_n,
                                       d_eps_z)*dh*b*fe.dx
                glass_ind += 1
            else:
                u_form += u_test.dx(0)*Es[i]*A[i]*u_tr.dx(0)*fe.dx
                u_form += phi_test.dx(0)*Es[i]*I[i]*phi_tr.dx(0)*fe.dx

            u_form += (w_test.dx(0) + phi_test)*Gs[i]*A[i]*(w_tr.dx(0) + phi_tr)*dx_shear
            u_form -= fe.Constant(0.0)*w_test*fe.dx

            if i < (n_lay - 1):
                lmbd_1_tr, lmbd_2_tr = lg_trial[ind_max + 2 * i], lg_trial[ind_max + 2 * i + 1]
                lmbd_1_test, lmbd_2_test = lg_test[ind_max + 2 * i], lg_test[ind_max + 2 * i + 1]
                ind_next = 3 * (i + 1)
                u_tr_next, w_tr_next, phi_tr_next = lg_trial[ind_next], lg_trial[ind_next + 1], lg_trial[ind_next + 2]
                u_test_next, w_test_next, phi_test_next = lg_test[ind_next], lg_test[ind_next + 1], lg_test[
                    ind_next + 2]

                u_form += lmbd_1_tr * (
                            u_test + 0.5 * h[i] * phi_test - u_test_next + 0.5 * h[i + 1] * phi_test_next) * fe.dx
                u_form += lmbd_1_test * (u_tr + 0.5 * h[i] * phi_tr - u_tr_next + 0.5 * h[i + 1] * phi_tr_next) * fe.dx
                u_form += lmbd_2_tr * (w_test - w_test_next) * fe.dx + lmbd_2_test * (w_tr - w_tr_next) * fe.dx

        return u_form

    # Return formulation for damage
    def get_d_form():
        d_i = fe.split(d)
        # d_tr = fe.TrialFunctions(self.W)
        d_test = fe.TestFunctions(W)

        d_form = 0.0
        for i in range(glass_num):
            d_form += -2*get_energy_active(u, d, 2*i)*fe.inner(1.0 - d_i[i], d_test[i])*fe.dx
            d_form += b*h[2*i]*3.0/8.0*Gc_glass*(1.0/lc*d_test[i] + 2*lc*fe.inner(fe.grad(d_i[i]), fe.grad(d_test[i])))*fe.dx

        return d_form

    def get_energy_active(x, d_, l_num):
        if type_active == "surface":
            lg_fce = fe.split(x)
            u_, w_, phi_ = lg_fce[3 * l_num], lg_fce[3 * l_num + 1], lg_fce[3 * l_num + 2]
            h_i = h[l_num]
            E = Es[l_num]
            eps_i_1 = u_.dx(0) + phi_.dx(0)*0.5*h_i
            eps_i_2 = u_.dx(0) - phi_.dx(0)*0.5*h_i
            eps_max = fu.max_fce(eps_i_1, eps_i_2)
            en = 0.5*fu.mc_bracket(eps_max)**2*E*h_i*b
        elif type_active == "energy" or type_active == "energy_mod":
            lg_fce = fe.split(x)
            u_, w_, phi_ = lg_fce[3 * l_num], lg_fce[3 * l_num + 1], lg_fce[3 * l_num + 2]
            en = 0.0
            hs = np.linspace(-0.5 * h[l_num], 0.5 * h[l_num], n_ni)
            dh = abs(hs[1] - hs[0])
            for j in range(len(hs) - 1):
                gauss = 0.5*(hs[j] + hs[j + 1])
                eps_z = u_.dx(0) + phi_.dx(0)*gauss
                eps_z_p = fu.mc_bracket(eps_z)
                temp = 0.5*Es[l_num]*eps_z_p**2*dh*b
                en += temp
        else:
            en = 0
        return en

    # --------------------
    # Parameters of task
    # --------------------
    # Material parameters
    E_glass = 70.0e9  # Young's modulus
    nu_glass = 0.22  # Poisson ratio
    G = vis_mat.Gs
    theta = vis_mat.thetas
    nu_vis = vis_mat.nu
    G_inf = vis_mat.Ginf
    c1 = vis_mat.c1
    c2 = vis_mat.c2
    T_ref = vis_mat.Tref
    T_act = vis_mat.Tact

    # Geometry parameters
    H1, H2, H3 = 0.01, 0.00076, 0.01
    h = [H1, H2, H3]
    l_x, l_y = 1.1, H1+H2+H3  # Length and thickness of glass beam
    l_off = 0.05  # Support offset
    l_0 = 0.2  # Pitch of load points
    b = 0.36
    n_lay = 3
    glass_num = 2
    n_ni = 20

    p_bottom = fe.Point((0.5*l_x, -0.5*h[0]))
    p_top = fe.Point((0.5*l_x, 0.5*h[2]))
    p_react = fe.Point(0.5 * l_x - 0.5 * l_0)

    # First initiation of G foil. The values are recalculated in each time step.
    G_foil = fe.Expression("g", g=0.0, degree=0)  # G value
    G_glass = E_glass/2/(1+nu_glass)
    E_foil = 2*G_foil*(1+nu_vis)

    Gs = [G_glass, G_foil, G_glass]
    Es = [E_glass, E_foil, E_glass]

    # --------------------
    # Define geometry
    # --------------------
    mesh = prepare_mesh()

    # Fracture parameters
    h_min = mesh.hmin()
    lc = 2*h_min
    Gc_glass = lc * 8.0 / 3.0 * ft ** 2 / E_glass

    if type_active == "energy_mod":
        Gc_glass = lc*8.0/3.0*ft**2/E_glass/6.0

    fe.plot(mesh)
    plt.show()

    # --------------------
    # Function spaces
    # --------------------
    p1 = fe.FiniteElement("P", fe.interval, 1)
    elems_num = 5*n_lay - 2
    element = fe.MixedElement(elems_num*[p1])
    V = fe.FunctionSpace(mesh, element)
    element_d = fe.MixedElement(glass_num*[p1])
    W = fe.FunctionSpace(mesh, element_d)
    V0 = fe.FunctionSpace(mesh, "DG", 0)

    #r_dof = fu.find_dof(p_react, 1, V)
    r_dof = fu.find_dof_beam(p_react, 7, V)

    # --------------------
    # Boundary conditions
    # --------------------
    u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
    BC_u_1 = fe.DirichletBC(V.sub(1), 0.0, left_point, method="pointwise")
    BC_u_2 = fe.DirichletBC(V.sub(7), u_d, left_load_point, method="pointwise")
    BC_u_3 = fe.DirichletBC(V.sub(0), 0.0, symmetry_axis)
    BC_u_4 = fe.DirichletBC(V.sub(2), 0.0, symmetry_axis)
    BC_u_5 = fe.DirichletBC(V.sub(6), 0.0, symmetry_axis)
    BC_u_6 = fe.DirichletBC(V.sub(8), 0.0, symmetry_axis)
    BC_u = [BC_u_1, BC_u_2, BC_u_3, BC_u_4, BC_u_5, BC_u_6]
    BC_d = []

    # --------------------
    # Initialization
    # --------------------
    u = fe.Function(V)
    u_old = fe.Function(V)
    du = fe.Function(V)
    d = fe.Function(W)
    d_old = fe.Function(W)
    dd = fe.Function(W)
    d_min = fe.Function(W)
    en_prev = 0.0

    file_u, file_d = prepare_files(folder, params, glass_num, n_lay)

    # --------------------
    # Forms
    # --------------------

    u_form = get_u_form()

    d_form = get_d_form()

    solve_staggered()


# Solver parameters
prms = fu.SolverParams()
prms.max_iter = 30
prms.tol_nm = 1.0e-11
prms.conv_crit = "rel_inc"

# Viscoelasticmaterial - EVA
eva_mat = fu.ViscoelasticMaterial()
eva_mat.Gs = 1.0e3 * np.array([6933.9, 3898.6, 2289.2, 1672.7, 761.6, 2401.0, 65.2, 248.0, 575.6, 56.3, 188.6, 445.1, 300.1, 401.6, 348.1, 111.6,
     127.2, 137.8, 50.5, 322.9, 100.0, 199.9])
eva_mat.thetas = np.array([1.0e-9, 1.0e-8, 1.0e-7, 1.0e-6, 1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5,
     1.0e6, 1.0e7, 1.0e8, 1.0e9, 1.0e10, 1.0e11, 1.0e12])
eva_mat.nu = 0.49
eva_mat.Ginf = 682.18e3
eva_mat.c1 = 339.102
eva_mat.c2 = 1185.816
eva_mat.Tref = 20.0
eva_mat.Tact = 25.0

# Viscoelasticmaterial - PVB
pvb_mat = fu.ViscoelasticMaterial()
pvb_mat.Gs = 1.0e3*np.array([1782124.2, 519208.7, 546176.8, 216893.2, 13618.3, 4988.3, 1663.8, 587.2, 258.0, 63.8, 168.4])
pvb_mat.thetas = np.array([1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5])
pvb_mat.nu = 0.49
pvb_mat.Ginf = 232.26e3
pvb_mat.c1 = 8.635
pvb_mat.c2 = 42.422
pvb_mat.Tref = 20.0
pvb_mat.Tact = 25.0

# Timespaces
time_space_eva_beam = fu.make_time_space([0.1, 7.0, 10.0], [0.1, 0.01, 0.001])
time_space_pvb_beam = fu.make_time_space([0.1, 7.0, 10.0], [0.1, 0.01, 0.001])
time_space_eva_beam_energy = fu.make_time_space([0.1, 14.0], [0.1, 0.01, 0.001])
time_space_pvb_beam_energy = fu.make_time_space([0.1, 19.0], [0.1, 0.01, 0.001])
time_space_eva_beam_energy_mod = fu.make_time_space([0.1, 10.0], [0.1, 0.01, 0.001])
time_space_pvb_beam_energy_mod = fu.make_time_space([0.1, 19.0], [0.1, 0.01, 0.001])

# Strengths of glass
ft_3l = 45.0e6

# Solution
# glass_3l_lg_solver(time_space_eva_beam, ft_3l, "Solutions/3L_eva_beam_45", prms, eva_mat)
# glass_3l_lg_solver(time_space_pvb_beam, ft_3l, "Solutions/3L_pvb_beam_45", prms, pvb_mat)
# glass_3l_lg_solver(time_space_eva_beam_energy, ft_3l, "Solutions/3L_eva_beam_45_energy", prms, eva_mat, "energy")
# glass_3l_lg_solver(time_space_pvb_beam_energy, ft_3l, "Solutions/3L_pvb_beam_45_energy", prms, pvb_mat, "energy")
# glass_3l_lg_solver(time_space_eva_beam_energy_mod, ft_3l, "Solutions/3L_eva_beam_45_energy_mod", prms, eva_mat, "energy_mod")
# glass_3l_lg_solver(time_space_pvb_beam_energy_mod, ft_3l, "Solutions/3L_pvb_beam_45_energy_mod", prms, pvb_mat, "energy_mod")

# Data loading
beam_eva_stress = np.loadtxt("Solutions/3L_eva_beam_45/stress_data.txt", skiprows=3)
beam_eva_react = np.loadtxt("Solutions/3L_eva_beam_45/reaction_data.txt", skiprows=2)
beam_eva_stress_energy = np.loadtxt("Solutions/3L_eva_beam_45_energy/stress_data.txt", skiprows=3)
beam_eva_react_energy = np.loadtxt("Solutions/3L_eva_beam_45_energy/reaction_data.txt", skiprows=2)
beam_eva_stress_energy_mod = np.loadtxt("Solutions/3L_eva_beam_45_energy_mod/stress_data.txt", skiprows=3)
beam_eva_react_energy_mod = np.loadtxt("Solutions/3L_eva_beam_45_energy_mod/reaction_data.txt", skiprows=2)

beam_pvb_stress = np.loadtxt("Solutions/3L_pvb_beam_45/stress_data.txt", skiprows=3)
beam_pvb_react = np.loadtxt("Solutions/3L_pvb_beam_45/reaction_data.txt", skiprows=2)
beam_pvb_stress_energy = np.loadtxt("Solutions/3L_pvb_beam_45_energy/stress_data.txt", skiprows=3)
beam_pvb_react_energy = np.loadtxt("Solutions/3L_pvb_beam_45_energy/reaction_data.txt", skiprows=2)
beam_pvb_stress_energy_mod = np.loadtxt("Solutions/3L_pvb_beam_45_energy_mod/stress_data.txt", skiprows=3)
beam_pvb_react_energy_mod = np.loadtxt("Solutions/3L_pvb_beam_45_energy_mod/reaction_data.txt", skiprows=2)

# Graph - EVA, Stress comparison
plt.title("Tensile stresses")
plt.plot(-1000*beam_eva_stress[:, 1], beam_eva_stress[:, 3]/1.0e6, label="EVA_beam", color="red")
plt.plot(-1000*beam_eva_stress_energy[:, 1], beam_eva_stress_energy[:, 3]/1.0e6, label="EVA_beam", color="red")
plt.plot(-1000*beam_eva_stress_energy_mod[:, 1], beam_eva_stress_energy_mod[:, 3]/1.0e6, "--", label="EVA_beam_mod", color="red")
plt.plot(-1000*beam_pvb_stress[:, 1], beam_pvb_stress[:, 3]/1.0e6, label="PVB_beam", color="blue")
plt.plot(-1000*beam_pvb_stress_energy[:, 1], beam_pvb_stress_energy[:, 3]/1.0e6, label="PVB_beam", color="blue")
plt.plot(-1000*beam_pvb_stress_energy_mod[:, 1], beam_pvb_stress_energy_mod[:, 3]/1.0e6, "--", label="PVB_beam", color="blue")
plt.legend()
plt.xlabel("Displacement w [mm]")
plt.ylabel("Stress [MPa]")
plt.show()

# Graph - EVA, Stress comparison
plt.title("Reaction")
plt.plot(-1000*beam_eva_react[:, 1], -2*0.36*beam_eva_react[:, 2]/1.0e3, label="EVA_beam", color="red")
plt.plot(-1000*beam_eva_react_energy[:, 1], -2*0.36*beam_eva_react_energy[:, 2]/1.0e3, label="EVA_beam", color="red")
plt.plot(-1000*beam_eva_react_energy_mod[:, 1], -2*0.36*beam_eva_react_energy_mod[:, 2]/1.0e3, "--", label="EVA_beam_mod", color="red")
plt.plot(-1000*beam_pvb_react[:, 1], -2*0.36*beam_pvb_react[:, 2]/1.0e3, label="PVB_beam", color="blue")
plt.plot(-1000*beam_pvb_react_energy[:, 1], -2*0.36*beam_pvb_react_energy[:, 2]/1.0e3, label="PVB_beam", color="blue")
plt.plot(-1000*beam_pvb_react_energy_mod[:, 1], -2*0.36*beam_pvb_react_energy_mod[:, 2]/1.0e3, "--", label="PVB_beam_mod", color="blue")
plt.legend()
plt.xlabel("Displacement w [mm]")
plt.ylabel("Reaction [kN]")
plt.show()
