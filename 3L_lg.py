import fenics as fe
import numpy as np
import matplotlib.pyplot as plt
import math
import os
import utils.functions as fu


# Mesh creator
def prepare_mesh(h_glass_i):
    Nx = 275
    Ny = 11

    # Geometry
    L = 1.1
    L_sym = 0.5 * L
    L1 = 0.45
    Hi = np.array([h_glass_i, 0.00076, h_glass_i])
    H = np.sum(Hi)
    n_lay = len(Hi)

    IF = np.zeros(n_lay)
    for i in range(n_lay):
        IF[i] = np.sum(Hi[0:i + 1])
    print(IF)

    # Uniform automatic mesh
    mesh = fe.RectangleMesh(fe.Point(0., 0.), fe.Point(L_sym, H), Nx, Ny, diagonal="left/right")

    cor = mesh.coordinates()
    cor_y = cor[:, 1]

    cor_y_val = np.linspace(0, H, Ny + 1)

    C = np.zeros(n_lay - 1)
    for i in range(n_lay - 1):
        idy = (np.abs(cor_y_val - IF[i])).argmin()
        C[i] = cor_y_val[idy]
        print(IF[i] - C[i])
        print(cor_y[idy])

    tol = H / Ny / 4 * 0.01

    # Move nodes to interface
    for iy in range(n_lay - 1):
        for ix in range(len(cor_y)):
            if fe.near(cor_y[ix], C[iy], tol):
                cor_y[ix] = IF[iy]
            for i in [1, 2, 3, 4]:
                if fe.near(cor_y[ix], i * H / Ny, tol):
                    cor_y[ix] = i * 0.002
            for i in [6, 7, 8, 9]:
                if fe.near(cor_y[ix], (i + 1) * H / Ny, tol):
                    cor_y[ix] = 0.00076 + i * 0.002

    # Refine near L/2
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if fe.near(f.midpoint()[0], L, (L - L1) + 3 * H / Ny):
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    # Refine near L/2 again
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if fe.near(f.midpoint()[0], L, (L - L1) + 2 * H / Ny):
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)

    # Refine near L/2 again
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near x == L/2
        for f in fe.facets(c):
            if fe.near(f.midpoint()[0], L, (L - L1) + 1 * H / Ny):
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)
    return mesh


def prepare_files(folder, params):
    # Post-process files
    # File for displacement field
    file_u = fe.XDMFFile(folder + "/u.xdmf")
    file_u.parameters["flush_output"] = True

    # File for damage field
    file_d = fe.XDMFFile(folder + "/d.xdmf")
    file_d.parameters["flush_output"] = True

    # Create log_file
    if not os.path.exists(folder):
        os.makedirs(folder)
    log_file = open(folder + "/log_file.txt", "w+")
    log_file.write("Log_file.txt\n")
    log_file.write(
        "eps_staggered = " + str(params.tol_sl) + ", max_iters_staggered = " + str(params.max_iter) +
        ", criterium = " + str(params.conv_crit) + ", True = converges\n")
    log_file.write("time\titer_numbers\tconverges\n")
    log_file.close()

    # Create stress_file
    stress_file = open(folder + "/stress_data.txt", "w+")
    stress_file.write("Stress_data.txt\n")
    stress_file.write(
        "S = Stress, E = Strain, T=Top surface in middle of beam, B=Bottom surface in middle of beam, TQ=Top surface in quarter ob beam\n")
    stress_file.write("time\tu_B\tE_x_B\tS_x_B\tE_x_T\tS_x_T\tE_x_QT\tS_x_QT\n")
    stress_file.close()

    # Create reacts_file
    react_file = open(folder + "/reaction_data.txt", "w+")
    react_file.write("Reaction_data.txt\n")
    react_file.write("time\tu_B\treaction\n")
    react_file.close()

    return file_u, file_d


def glass_3l_lg_solver(time_space, ft, h_glass, folder, params, vis_mat):

    # Definition of supporting point
    def left_point(x):
        return fe.near(x[0], l_off) and fe.near(x[1], 0.0)

    # Definition of loading point
    def left_load_point(x):
        return fe.near(x[0], l_x / 2 - l_0 / 2) and fe.near(x[1], l_y)

    # Definition of symmetry axis location
    def symmetry_axis(x, on_boundary):
        return fe.near(x[0], l_x/2) and on_boundary

    # Elastic stress tensor
    def sigma_el(u_i):
        return lmbda*(fe.tr(fu.eps(u_i)))*fe.Identity(2) + 2.0*mu*(fu.eps(u_i))

    # Positive stress tensor - spectral decomposition
    def sigma_p_sd(u_i):
        return lmbda*(fe.tr(fu.eps_p(u_i)))*fe.Identity(2) + 2.0*mu*(fu.eps_p(u_i))

    # Negative stress tensor - spectral decomposition
    def sigma_n_sd(u_i):
        return lmbda*(fe.tr(fu.eps_n(u_i)))*fe.Identity(2) + 2.0*mu*(fu.eps_n(u_i))

    # Positive energy - spectral decomposition
    def psi_sd_plus(u_i):
        eps_sym = fu.eps(u_i)

        eps1 = (1./2.)*fe.tr(eps_sym) + fe.sqrt((1./4.)*(fe.tr(eps_sym)**2) - fe.det(eps_sym))
        eps2 = (1./2.)*fe.tr(eps_sym) - fe.sqrt((1./4.)*(fe.tr(eps_sym)**2) - fe.det(eps_sym))

        tr_eps_plus = fu.mc_bracket(fe.tr(eps_sym))
        sum_eps_plus_squared = fu.mc_bracket(eps1)**2 + fu.mc_bracket(eps2)**2

        return 0.5*lmbda*tr_eps_plus**2 + mu*sum_eps_plus_squared

    # Negative energy - spectral decomposition
    def psi_sd_minus(u_i):
        eps_sym = fu.eps(u_i)

        eps1 = (1./2.)*fe.tr(eps_sym) + fe.sqrt((1./4.)*(fe.tr(eps_sym)**2) - fe.det(eps_sym))
        eps2 = (1./2.)*fe.tr(eps_sym) - fe.sqrt((1./4.)*(fe.tr(eps_sym)**2) - fe.det(eps_sym))

        tr_eps_minus = -fu.mc_bracket(-fe.tr(eps_sym))
        sum_eps_minus_squared = fu.mc_bracket(-eps1)**2 + fu.mc_bracket(-eps2)**2

        return 0.5*lmbda*tr_eps_minus**2 + mu*sum_eps_minus_squared

    # Total energy of system - getter
    def get_total_energy(u_i, d_i):
        en_el_dens_1 = (1 - d_exp_ex)**2*psi_sd_plus(u_i) + psi_sd_minus(u_i)
        en_el_dens_2 = 3.0/8.0*Gc_glass*(1.0/lc*d_i + lc*fe.inner(fe.grad(d_i), fe.grad(d_i)))
        return fe.assemble(en_el_dens_1*fe.dx) + fe.assemble(en_el_dens_2*fe.dx)

    # Staggered loop
    def solve_staggered():
        # Time loop
        en_prev = 0.0
        for i in range(0, len(time_space)):
            t = time_space[i]

            u_d.t = t

            # Update of foil stiffness
            update_foil(t)

            # Informative print of time instant
            print("Time instant: ", t)

            ite = 1
            err = 50.0

            converges = False

            # Staggered loop
            while err > params.tol_sl:

                map_d_to_d_exp(t)

                # Damage and displacement solutions
                solve_displacement()

                u_sub.assign(fe.project(u, V_sub))

                solve_damage()

                if params.conv_crit == "rel_inc":
                    # Damage and displacement increments
                    du.assign(u - u_old)
                    dd.assign(d - d_old)

                    # Errors - damage is not standardized
                    err_u = fe.norm(du) / fe.norm(u)
                    err_d = fe.norm(dd)
                    err = max(err_u, err_d)

                    print("iter", ite, "errors", err_u, err_d)

                    u_old.assign(u)
                    d_old.assign(d)
                elif params.conv_crit == "energy":
                    en = get_total_energy(u, d)
                    err = abs((en - en_prev) / en)
                    print("iter", ite, ", energy_error: ", err)
                    en_prev = en
                else:
                    raise Exception("conv_crit must be energy/rel_inc!")

                ite += 1

                # Max iterations condition
                if ite > params.max_iter:
                    print("max iterations reached")
                    break

                if err <= params.tol_sl:
                    converges = True

            d_min.assign(d)
            file_u.write(u, t)
            file_d.write(d, t)

            save_stress_reaction_sd(t)

            file_object = open(folder + "/log_file.txt", "a")
            file_object.write(str(round(t, 5)) + "\t" + str(ite) + "\t" + str(converges) + "\n")
            file_object.close()

        file_u.close()
        file_d.close()

    # Displacement solver
    def solve_displacement():
        u.vector()[:] = np.random.rand(u.vector().size())
        # positive_part = (1-d_exp_ex)**2*psi_sd_plus(u)*fe.dx
        # negative_part = psi_sd_minus(u)*fe.dx
        # F = fe.derivative(positive_part + negative_part, u, fe.TestFunction(V))
        F = fe.derivative(psi_u_form, u, fe.TestFunction(V))
        H = fe.derivative(F, u, fe.TrialFunction(V))
        problem = fe.NonlinearVariationalProblem(F, u, BC_u, H)
        solver = fe.NonlinearVariationalSolver(problem)
        parameters = {"newton_solver": {"error_on_nonconvergence": False,
                                        "relative_tolerance": params.tol_nm}}
        solver.parameters.update(parameters)
        solver.solve()

    # Damage snes variational inequalities solver based on Newton's method
    def solve_damage():
        lower = d_min
        upper = fe.interpolate(fe.Constant(1.0), W_sub)

        # Solution of damage formulationw
        H = fe.derivative(d_form, d, fe.TrialFunction(W_sub))

        snes_solver_parameters = {"nonlinear_solver": "snes",
                                  "snes_solver": {"linear_solver": "lu",
                                                  "relative_tolerance": 1.0e-6,
                                                  "absolute_tolerance": 1.0e-6,
                                                  "maximum_iterations": 50,
                                                  "report": True,
                                                  "error_on_nonconvergence": False,
                                                  "line_search": "basic"}}

        problem = fe.NonlinearVariationalProblem(d_form, d, BC_d, H)
        problem.set_bounds(lower, upper)

        solver = fe.NonlinearVariationalSolver(problem)
        solver.parameters.update(snes_solver_parameters)
        solver.solve()

    # Save stress and reaction to file
    def save_stress_reaction_sd(t_i):
        stress_file = open(folder + "/stress_data.txt", "a")
        strain = fu.local_project(fu.eps(u), V0)
        # stress = fu.local_project((1-d_exp_ex)**2*sigma_el(u), V0)
        stress = fu.local_project((1-d_exp_ex)**2*sigma_p_sd(u) + sigma_n_sd(u), V0)
        s1 = strain(p_bottom)[0]
        st1 = stress(p_bottom)[0]
        s2 = strain(p_top)[0]
        st2 = stress(p_top)[0]
        s3 = strain(p_top_q)[0]
        st3 = stress(p_top_q)[0]
        disp = u(p_top)[1]
        stress_file.write(str(t_i) + "\t" + str(disp) + "\t" + str(s1) + "\t" + str(st1) + "\t" + str(s2) + "\t" + str(st2) + "\t" + str(s3) + "\t" + str(st3) + "\n")
        stress_file.close()
        react_file = open(folder + "/reaction_data.txt", "a")
        positive_part = (1-d_exp_ex)**2*psi_sd_plus(u)*fe.dx
        negative_part = psi_sd_minus(u)*fe.dx
        F = fe.derivative(positive_part + negative_part, u, fe.TestFunction(V))
        forces = fe.assemble(F)
        react = forces[r_dof]
        react_file.write(str(t_i) + "\t" + str(disp) + "\t" + str(react) + "\n")
        react_file.close()

    # Map damage field onto whole mesh
    def map_d_to_d_exp(t_i):
        dofb_vb = np.array(fe.dof_to_vertex_map(W_sub), dtype=int)
        vb_v = submesh_L1.data().array("parent_vertex_indices", 0)
        v_dof = np.array(fe.vertex_to_dof_map(W), dtype=int)

        d_exp.vector()[v_dof[vb_v[dofb_vb]]] = d.vector()[:]
        d_exp_ex.glass = d_exp

    # Calculate new lambda and mu for foil
    def update_foil(t_i):
        G_foil_t = get_G(t_i*60.0/1.8/2)
        lmbda_temp = 2.0*G_foil_t*nu_vis/(1.0 - 2.0*nu_vis)
        lmbda_temp = 2*G_foil_t*lmbda_temp/(lmbda_temp + 2*G_foil_t)
        lmbda.foil = lmbda_temp
        mu.foil = G_foil_t

    # Get foil stiffness for given time t_i
    def get_G(t_i):
        a_t = math.pow(10.0, -c1*(T_act - T_ref)/(c2 + T_act - T_ref))
        temp = G_inf
        n = len(G)
        temp += sum(G[k]*math.exp(-t_i/(a_t*theta[k])) for k in range(0, n))
        return temp

    # --------------------
    # Parameters of task
    # --------------------
    # Material parameters
    E_glass = 70.0e9  # Young's modulus
    nu_glass = 0.22  # Poisson ratio
    G = vis_mat.Gs
    theta = vis_mat.thetas
    nu_vis = vis_mat.nu
    G_inf = vis_mat.Ginf
    c1 = vis_mat.c1
    c2 = vis_mat.c2
    T_ref = vis_mat.Tref
    T_act = vis_mat.Tact

    # Fracture parameters
    # TODO: ??
    lc = 0.0003
    Gc_glass = lc * 8.0 / 3.0 * ft ** 2 / E_glass

    # Geometry parameters
    H1, H2, H3 = h_glass, 0.00076, h_glass
    l_x, l_y = 1.1, H1+H2+H3  # Length and thickness of glass beam
    l_off = 0.05  # Support offset
    l_0 = 0.2  # Pitch of load points

    p_bottom = fe.Point(0.5*l_x, 0.0)
    p_top = fe.Point(0.5*l_x, l_y)
    p_top_q = fe.Point(0.3, l_y)
    p_react = fe.Point((0.5 * l_x - 0.5 * l_0, l_y))

    # Lames coefficients calculation
    lambda_glass = E_glass*nu_glass/(1 + nu_glass)/(1 - 2*nu_glass)
    mu_glass = E_glass/2/(1 + nu_glass)
    lambda_glass = 2*mu_glass*lambda_glass/(lambda_glass + 2*mu_glass)

    # First initiation of lambda and mu of foil. The values are recalculated in each time step.
    lambda_foil = lambda_glass
    mu_foil = mu_glass

    # --------------------
    # Define geometry
    # --------------------
    mesh = prepare_mesh(h_glass)

    # Submeshes
    m_function = fe.MeshFunction("size_t", mesh, 2, 0)
    bottom_glass = fe.AutoSubDomain(lambda x: abs(x[1] - 0.5*l_y) > 0.5*H2 - fe.DOLFIN_EPS)
    bottom_glass.mark(m_function, 1)
    submesh_L1 = fe.SubMesh(mesh, m_function, 1)

    fe.plot(submesh_L1)
    plt.show()

    fe.plot(mesh)
    plt.show()

    # --------------------
    # Function spaces
    # --------------------
    V = fe.VectorFunctionSpace(mesh, "CG", 1)  # Function space for displacements
    V0 = fe.TensorFunctionSpace(mesh, "DG", 0)  # Function space for stress components
    V_sub = fe.VectorFunctionSpace(submesh_L1, "CG", 1)  # Function space for displacement on submesh
    W = fe.FunctionSpace(mesh, "CG", 1)  # Function space for damage
    W_sub = fe.FunctionSpace(submesh_L1, "CG", 1)  # Function space for damage on submesh

    r_dof = fu.find_dof(p_react, 1, V)

    # --------------------
    # Boundary conditions
    # --------------------
    u_d = fe.Expression("-t/1000.0", t=0.0, degree=0)  # Prescribed displacement
    BC_u_1 = fe.DirichletBC(V.sub(1), 0.0, left_point, method="pointwise")
    BC_u_2 = fe.DirichletBC(V.sub(1), u_d, left_load_point, method="pointwise")
    BC_u_3 = fe.DirichletBC(V.sub(0), 0.0, symmetry_axis)
    BC_u = [BC_u_1, BC_u_2, BC_u_3]
    BC_d = []

    # --------------------
    # Initialization
    # --------------------
    u = fe.Function(V)
    u_sub = fe.Function(V_sub)
    u_old = fe.Function(V)
    du = fe.Function(V)
    d = fe.Function(W_sub)
    d_exp = fe.Function(W)
    d_old = fe.Function(W_sub)
    dd = fe.Function(W_sub)
    d_min = fe.Function(W_sub)
    en_prev = 0.0

    file_u, file_d = prepare_files(folder, params)

    lmbda = fe.Expression("abs(x[1]-H1-0.5*H2) < 0.5*H2 ? foil : glass", H1=H1, H2=H2, foil=lambda_foil, glass=lambda_glass, t=1.0, degree=0)
    mu = fe.Expression("abs(x[1]-H1-0.5*H2) < 0.5*H2 ? foil : glass", H1=H1, H2=H2, foil=mu_foil, glass=mu_glass, t=1.0, degree=0)
    Gc = fe.Expression("abs(x[1]-H1-0.5*H2) < 0.5*H2 ? foil : glass", H1=H1, H2=H2, foil=Gc_glass*1000.0, glass=Gc_glass, t=1.0, degree=0)
    d_exp_ex = fe.Expression("abs(x[1]-H1-0.5*H2) < 0.5*H2 ? foil : glass", H1=H1, H2=H2, foil=0.0, glass=d, t=1.0, degree=1)

    d_init_expression = fe.Expression("x[1] < H1 + 0.0001 && abs(x[0] - 0.5*l_x + 0.4*l_0) < lc ? 0.99 : 0.0", H1=H1, l_x=l_x, l_0=l_0, lc=lc, degree=0)
    d_init_expression_2 = fe.Expression("x[1] < H1 + 0.0001 && (abs(x[0] - 0.5*l_x + 0.4*l_0) < lc || abs(x[0] - 0.5*l_x + 0.3*l_0) < lc || abs(x[0] - 0.5*l_x + 0.2*l_0) < lc) ? 0.99 : 0.0", H1=H1, l_x=l_x, l_0=l_0, lc=lc, degree=0)
    d_init_expression_3 = fe.Expression("x[1] < H1 + 0.0001 && (abs(x[0] - 0.5*l_x + 0.4*l_0) < lc || abs(x[0] - 0.5*l_x + 0.3*l_0) < lc || abs(x[0] - 0.5*l_x + 0.2*l_0) < lc || abs(x[0] - 0.5*l_x + 0.45*l_0) < lc || abs(x[0] - 0.5*l_x + 0.35*l_0) < lc || abs(x[0] - 0.5*l_x + 0.25*l_0) < lc ) ? 0.99 : 0.0", H1=H1, l_x=l_x, l_0=l_0, lc=lc, degree=0)
    d_init = fe.interpolate(d_init_expression, W_sub)
    d_init_2 = fe.interpolate(d_init_expression_2, W_sub)
    d_init_3 = fe.interpolate(d_init_expression_3, W_sub)
    #d_min = d_init_3

    # --------------------
    # Forms
    # --------------------
    d_test = fe.TestFunction(W_sub)

    d_form = -2*psi_sd_plus(u_sub)*fe.inner(1.0 - d, d_test)*fe.dx  # Damage formulation with spectral-decomposition split
    d_form += 3.0/8.0*Gc*(1.0/lc*d_test + 2*lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
    psi_u_form = (1 - d_exp_ex) ** 2 * psi_sd_plus(u) * fe.dx + psi_sd_minus(u) * fe.dx

    solve_staggered()


# Solver parameters
prms = fu.SolverParams()
prms.max_iter = 200
prms.tol_nm = 1.0e-11

# Viscoelasticmaterial - EVA
eva_mat = fu.ViscoelasticMaterial()
eva_mat.Gs = 1.0e3 * np.array([6933.9, 3898.6, 2289.2, 1672.7, 761.6, 2401.0, 65.2, 248.0, 575.6, 56.3, 188.6, 445.1, 300.1, 401.6, 348.1, 111.6,
     127.2, 137.8, 50.5, 322.9, 100.0, 199.9])
eva_mat.thetas = np.array([1.0e-9, 1.0e-8, 1.0e-7, 1.0e-6, 1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5,
     1.0e6, 1.0e7, 1.0e8, 1.0e9, 1.0e10, 1.0e11, 1.0e12])
eva_mat.nu = 0.49
eva_mat.Ginf = 682.18e3
eva_mat.c1 = 339.102
eva_mat.c2 = 1185.816
eva_mat.Tref = 20.0
eva_mat.Tact = 25.0

# Viscoelasticmaterial - PVB
pvb_mat = fu.ViscoelasticMaterial()
pvb_mat.Gs = 1.0e3*np.array([1782124.2, 519208.7, 546176.8, 216893.2, 13618.3, 4988.3, 1663.8, 587.2, 258.0, 63.8, 168.4])
pvb_mat.thetas = np.array([1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5])
pvb_mat.nu = 0.49
pvb_mat.Ginf = 232.26e3
pvb_mat.c1 = 8.635
pvb_mat.c2 = 42.422
pvb_mat.Tref = 20.0
pvb_mat.Tact = 25.0

# Timespaces
time_space_eva_min = fu.make_time_space([0.1, 5.0, 5.35, 5.36], [0.1, 0.01, 0.001])
time_space_eva_min_mod = fu.make_time_space([0.1, 5.0, 5.45, 6.5], [0.1, 0.01, 0.001])
time_space_eva_min_fine = fu.make_time_space([0.1, 5.0, 5.35, 5.356, 5.36], [0.1, 0.01, 0.001, 0.00001])
time_space_eva_max = fu.make_time_space([0.1, 9.0, 10.0, 10.45, 10.46], [0.5, 0.1, 0.01, 0.001])
time_space_pvb_min = fu.make_time_space([0.1, 5.0, 6.0, 6.25, 6.27], [0.5, 0.1, 0.01, 0.001])
time_space_pvb_max = fu.make_time_space([0.1, 14.0, 15.0, 15.7, 15.8], [0.5, 0.1, 0.01, 0.001])

# Strengths of glass
ft_min_eva = 31.0e6
ft_min_eva_mod = 32.0e6
ft_max_eva = 60.0e6
ft_min_pvb = 28.0e6
ft_max_pvb = 69.0e6

# Solution
# glass_3l_lg_solver(time_space_eva_min_mod, ft_min_eva_mod, 0.01, "Solutions/3L_eva_min_mod", prms, eva_mat)
# glass_3l_lg_solver(time_space_eva_min_fine, ft_min_eva, 0.01, "Solutions/3L_eva_min_fine", prms, eva_mat)
# glass_3l_lg_solver(time_space_eva_max, ft_max_eva, 0.01, "Solutions/3L_eva_max", prms, eva_mat)
# glass_3l_lg_solver(time_space_pvb_min, ft_min_pvb, 0.01, "Solutions/3L_pvb_min", prms, pvb_mat)
# glass_3l_lg_solver(time_space_pvb_max, ft_max_pvb, 0.01, "Solutions/3L_pvb_max", prms, pvb_mat)

# Data loading - EVA
eva_min_stress = np.loadtxt("Solutions/3L_eva_min/stress_data.txt", skiprows=3)
eva_min_reacts = np.loadtxt("Solutions/3L_eva_min/reaction_data.txt", skiprows=2)
eva_max_stress = np.loadtxt("Solutions/3L_eva_max/stress_data.txt", skiprows=3)
eva_max_reacts = np.loadtxt("Solutions/3L_eva_max/reaction_data.txt", skiprows=2)

# Data loading - EVA experiments
eva_exp_01 = np.loadtxt("Data/VSG_EVA_01-1_red.txt")
eva_exp_02 = np.loadtxt("Data/VSG_EVA_02-2_red.txt")
eva_exp_03 = np.loadtxt("Data/VSG_EVA_03-1_red.txt")
eva_exp_04 = np.loadtxt("Data/VSG_EVA_04-1_red.txt")
eva_exp_05 = np.loadtxt("Data/VSG_EVA_05-1_red.txt")

# Data loading - PVB
pvb_min_stress = np.loadtxt("Solutions/3L_pvb_min/stress_data.txt", skiprows=3)
pvb_min_reacts = np.loadtxt("Solutions/3L_pvb_min/reaction_data.txt", skiprows=2)
pvb_max_stress = np.loadtxt("Solutions/3L_pvb_max/stress_data.txt", skiprows=3)
pvb_max_reacts = np.loadtxt("Solutions/3L_pvb_max/reaction_data.txt", skiprows=2)

# Data loading - PVB experiments
pvb_exp_01 = np.loadtxt("Data/VSG_PVB_02-1_red.txt")
pvb_exp_02 = np.loadtxt("Data/VSG_PVB_03-1_red.txt")
pvb_exp_03 = np.loadtxt("Data/VSG_PVB_04-1_red.txt")
pvb_exp_04 = np.loadtxt("Data/VSG_PVB_06-1_red.txt")
pvb_exp_05 = np.loadtxt("Data/VSG_PVB_07-1_red.txt")

# Graph - EVA, Stress comparison
plt.title("3L-PS - tensile stresses")
plt.plot(-1000*eva_min_stress[:, 1], eva_min_stress[:, 3]/1.0e6, label="EVA_min", color="red")
plt.plot(-1000*eva_max_stress[:, 1], eva_max_stress[:, 3]/1.0e6, label="EVA_max", color="blue")
plt.plot(0.5*eva_exp_01[:, 1] + 0.5*eva_exp_01[:, 2], eva_exp_01[:, 5], label="EVA_exp", color="gray")
plt.plot(0.5*eva_exp_02[:, 1] + 0.5*eva_exp_02[:, 2], eva_exp_02[:, 5], color="gray")
plt.plot(0.5*eva_exp_03[:, 1] + 0.5*eva_exp_03[:, 2], eva_exp_03[:, 5], color="gray")
plt.plot(0.5*eva_exp_04[:, 1] + 0.5*eva_exp_04[:, 2], eva_exp_04[:, 5], color="gray")
plt.plot(0.5*eva_exp_05[:, 1] + 0.5*eva_exp_05[:, 2], eva_exp_05[:, 5], color="gray")
plt.plot(0.5*eva_exp_01[:, 1] + 0.5*eva_exp_01[:, 2], eva_exp_01[:, 4], color="gray")
plt.plot(0.5*eva_exp_02[:, 1] + 0.5*eva_exp_02[:, 2], eva_exp_02[:, 4], color="gray")
plt.plot(0.5*eva_exp_03[:, 1] + 0.5*eva_exp_03[:, 2], eva_exp_03[:, 4], color="gray")
plt.plot(0.5*eva_exp_04[:, 1] + 0.5*eva_exp_04[:, 2], eva_exp_04[:, 4], color="gray")
plt.plot(0.5*eva_exp_05[:, 1] + 0.5*eva_exp_05[:, 2], eva_exp_05[:, 4], color="gray")
plt.plot(0.5*eva_exp_01[:, 1] + 0.5*eva_exp_01[:, 2], eva_exp_01[:, 6], color="gray")
plt.plot(0.5*eva_exp_02[:, 1] + 0.5*eva_exp_02[:, 2], eva_exp_02[:, 6], color="gray")
plt.plot(0.5*eva_exp_03[:, 1] + 0.5*eva_exp_03[:, 2], eva_exp_03[:, 6], color="gray")
plt.plot(0.5*eva_exp_04[:, 1] + 0.5*eva_exp_04[:, 2], eva_exp_04[:, 6], color="gray")
plt.plot(0.5*eva_exp_05[:, 1] + 0.5*eva_exp_05[:, 2], eva_exp_05[:, 6], color="gray")
plt.legend()
plt.xlabel("Displacement w [mm]")
plt.ylabel("Stress [MPa]")
plt.show()

# Graph - EVA, Reactions comparison
plt.title("3L-PS - reactions")
plt.plot(-1000*eva_min_reacts[:, 1], -2*0.36*eva_min_reacts[:, 2]/1.0e3, label="EVA_min")
plt.plot(-1000*eva_max_reacts[:, 1], -2*0.36*eva_max_reacts[:, 2]/1.0e3, label="EVA_min")
plt.plot(0.5*eva_exp_01[:, 1] + 0.5*eva_exp_01[:, 2], -eva_exp_01[:, 3], label="EVA_exp", color="gray")
plt.plot(0.5*eva_exp_02[:, 1] + 0.5*eva_exp_02[:, 2], -eva_exp_02[:, 3], color="gray")
plt.plot(0.5*eva_exp_03[:, 1] + 0.5*eva_exp_03[:, 2], -eva_exp_03[:, 3], color="gray")
plt.plot(0.5*eva_exp_04[:, 1] + 0.5*eva_exp_04[:, 2], -eva_exp_04[:, 3], color="gray")
plt.plot(0.5*eva_exp_05[:, 1] + 0.5*eva_exp_05[:, 2], -eva_exp_05[:, 3], color="gray")
plt.legend()
plt.xlabel("Displacement w [mm]")
plt.ylabel("Reaction [kN]")
plt.show()

# Graph - PVB, Stress comparison
plt.title("3L-PS - tensile stresses")
plt.plot(-1000*pvb_min_stress[:, 1], pvb_min_stress[:, 3]/1.0e6, label="PVB_min")
plt.plot(-1000*pvb_max_stress[:, 1], pvb_max_stress[:, 3]/1.0e6, label="PVB_max")
plt.plot(0.5*pvb_exp_01[:, 1] + 0.5*pvb_exp_01[:, 2], pvb_exp_01[:, 5], label="PVB_exp", color="gray")
plt.plot(0.5*pvb_exp_02[:, 1] + 0.5*pvb_exp_02[:, 2], pvb_exp_02[:, 5], color="gray")
plt.plot(0.5*pvb_exp_03[:, 1] + 0.5*pvb_exp_03[:, 2], pvb_exp_03[:, 5], color="gray")
plt.plot(0.5*pvb_exp_04[:, 1] + 0.5*pvb_exp_04[:, 2], pvb_exp_04[:, 5], color="gray")
plt.plot(0.5*pvb_exp_05[:, 1] + 0.5*pvb_exp_05[:, 2], pvb_exp_05[:, 5], color="gray")
plt.plot(0.5*pvb_exp_01[:, 1] + 0.5*pvb_exp_01[:, 2], pvb_exp_01[:, 4], color="gray")
plt.plot(0.5*pvb_exp_02[:, 1] + 0.5*pvb_exp_02[:, 2], pvb_exp_02[:, 4], color="gray")
plt.plot(0.5*pvb_exp_03[:, 1] + 0.5*pvb_exp_03[:, 2], pvb_exp_03[:, 4], color="gray")
plt.plot(0.5*pvb_exp_04[:, 1] + 0.5*pvb_exp_04[:, 2], pvb_exp_04[:, 4], color="gray")
plt.plot(0.5*pvb_exp_05[:, 1] + 0.5*pvb_exp_05[:, 2], pvb_exp_05[:, 4], color="gray")
plt.plot(0.5*pvb_exp_01[:, 1] + 0.5*pvb_exp_01[:, 2], pvb_exp_01[:, 6], color="gray")
plt.plot(0.5*pvb_exp_02[:, 1] + 0.5*pvb_exp_02[:, 2], pvb_exp_02[:, 6], color="gray")
plt.plot(0.5*pvb_exp_03[:, 1] + 0.5*pvb_exp_03[:, 2], pvb_exp_03[:, 6], color="gray")
plt.plot(0.5*pvb_exp_04[:, 1] + 0.5*pvb_exp_04[:, 2], pvb_exp_04[:, 6], color="gray")
plt.plot(0.5*pvb_exp_05[:, 1] + 0.5*pvb_exp_05[:, 2], pvb_exp_05[:, 6], color="gray")
plt.legend()
plt.xlabel("Displacement w [mm]")
plt.ylabel("Stress [MPa]")
plt.show()

# Graph - PVB, Reactions comparison
plt.title("3L-PS - reactions")
plt.plot(-1000*pvb_min_reacts[:, 1], -2*0.36*pvb_min_reacts[:, 2]/1.0e3, label="PVB_min")
plt.plot(-1000*pvb_max_reacts[:, 1], -2*0.36*pvb_max_reacts[:, 2]/1.0e3, label="PVB_min")
plt.plot(0.5*pvb_exp_01[:, 1] + 0.5*pvb_exp_01[:, 2], -pvb_exp_01[:, 3], label="PVB_exp", color="gray")
plt.plot(0.5*pvb_exp_02[:, 1] + 0.5*pvb_exp_02[:, 2], -pvb_exp_02[:, 3], color="gray")
plt.plot(0.5*pvb_exp_03[:, 1] + 0.5*pvb_exp_03[:, 2], -pvb_exp_03[:, 3], color="gray")
plt.plot(0.5*pvb_exp_04[:, 1] + 0.5*pvb_exp_04[:, 2], -pvb_exp_04[:, 3], color="gray")
plt.plot(0.5*pvb_exp_05[:, 1] + 0.5*pvb_exp_05[:, 2], -pvb_exp_05[:, 3], color="gray")
plt.legend()
plt.xlabel("Displacement w [mm]")
plt.ylabel("Reaction [kN]")
plt.show()
